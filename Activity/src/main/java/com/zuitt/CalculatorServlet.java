package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	private static final long serialVersionUID = 43283931101411089L;
	
		public void init() throws ServletException {
			System.out.println("*****************************");
			System.out.println("Initialized connection to DB");
			System.out.println("*****************************");
		}
		
		public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
			int num1 = Integer.parseInt(req.getParameter("num1"));
			int num2 = Integer.parseInt(req.getParameter("num2"));
			String operation = req.getParameter("operation");
			int answer = 0;

			if (operation != null) {
				switch (operation) {
					case "add":
						answer = num1 + num2;
						break;
					case "subtract":
						answer = num1 - num2;
						break;
					case "multiply":
						answer = num1 * num2;
						break;
					case "divide":
						answer = num1 / num2;
						break;
					default:
						break;
				}
			}

			PrintWriter out = res.getWriter();
			out.println("The two numbers you provided are: "+ num1 +","+num2);
			out.println("The operation that you wanted is: "+operation);
			out.println("The result is: "+answer);
		}

		public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {

			PrintWriter out = res.getWriter();
			out.println("<h1>You are now using the calculator app</h1>");
			out.println("To use the app, input two numbers and an operation. <br><br>");
			out.println("Hit the submit button after filling in the details. <br><br>");
			out.println("You will get the result shown in your browser!");
		}

		public void destroy() {
			System.out.println("***********************");
			System.out.println("Disconnected from DB.");
			System.out.println("***********************");
		}

}
